<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send_email extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

        $this->load->library('grocery_CRUD');
        // $this->load->model('admin/auth_model', 'auth_model');
        $this->load->library('email');

        // $this->load->model('itasset/Vendor_model', 'Vendor_model');
        $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
	}


   public function email_alert(){
    $config = Array(
        'protocol'  => 'smtp',
        'smtp_host' => 'mail.service-imsp.com',
        'smtp_port' => 25,
        'smtp_user' => 'qrcode_inventory@service=imsp.com',
        'smtp_pass' => '123456',
        'mailtype'  => 'html',
        'charset'   => 'utf-8',
        'smtp_crypto' => "tls"
        );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->set_mailtype("html");
            $this->email->from('qrcode_inventory@service-imsp.com', 'Automatic System Email QRCode Inventory');

                // $data = array(

                // 'userName'=> 'Akarapon Nukoonwuitiopas'
         
                // );
                $this->load->model('itasset/item_asset_model', 'item_asset_model');
                $data['asset_expired_1'] =  $this->item_asset_model->get_list_asset_expired_in_1();

                $this->load->model('itasset/item_asset_model', 'item_asset_model');
                $data['asset_expired_2'] =  $this->item_asset_model->get_list_asset_expired_in_2();

                $this->load->model('itasset/item_asset_model', 'item_asset_model');
                $data['asset_expired_3'] =  $this->item_asset_model->get_list_asset_expired_in_3();


            $this->email->to('akarapon.n@gmail.com');
            $this->email->subject('Email List Asset Expire in 0-90 Days');
            // $this->email->message('Please login to check your new update. Do not reply to this email. For any issues reach out to email@emailid.com');
            $body = $this->load->view('emails/expire_alert.php',$data,TRUE);

            $this->email->message($body); 

                    if (!$this->email->send())
                    {
                        show_error($this->email->print_debugger());
                    }
                    else
                    {
                        echo true;
                    }
   }


	

}