<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin:*');

class Mobile extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('admin/auth_model', 'auth_model');
		// $this->load->model('admin/Item_model', 'Item_model');
        // $this->load->model('admin/transaction_model', 'transaction_model');
    }

	public function index()
	{
		redirect(base_url('admin'));
    }

	public function testpost(){
		echo "Result-Post ";
		$username = $this->input->post('username');
        $password = $this->input->post('password');
		echo $username;
		echo " - ";
		echo $password;
		//var_dump($this->input->raw_input_stream);
	}
    
	public function testget(){
		echo "Result-Get ";
		$username = $this->input->get('username');
        $password = $this->input->get('password');
		echo $username;
		echo " - ";
		echo $password;
	}
    

    public function login(){

        $username = $this->input->get('username');
        $password = $this->input->get('password');
        
        //echo $_POST[0];
        //print_r($this->input->raw_input_stream);
        //print_r($_post);

        $data = array(
            'username' => $username,
            'password' => $password 
            );

			/*
            $result = $this->auth_model->login($data);
            if ($result == TRUE) {
                $admin_data = array(
                    'admin_id' => $result['id'],
                     'name' => $result['username'],
                     // add warehouse and user profile set session
                     'whse' => $result['center_code'],
                     'email' => $result['email'],
                     'firstname' => $result['firstname'],
                     'lastname' => $result['lastname'],
                     'role' => $result['role']
                    //  'is_admin_login' => TRUE
                );

                echo json_encode($admin_data);
            }
            else{
                $msg = 'Invalid Email or Password!!!';
                echo $msg;
            }
			*/
			$result = $this->auth_model->login($data);
			  $admin_data = array(
                    'admin_id' => $result['id'],
                     'name' => $result['username'],
                     // add warehouse and user profile set session
                     'whse' => $result['center_code'],
                     'email' => $result['email'],
                     'firstname' => $result['firstname'],
                     'lastname' => $result['lastname'],
                     'role' => $result['role']
                    //  'is_admin_login' => TRUE
                );

                echo json_encode($admin_data);
    }


	public function getitem(){
	    $item_barcode = $this->input->get('barcode_text');
		$whse_center_code = $this->input->get('whse');
    
        $data = array(
            'item_barcode' => $item_barcode,
            'whse_center_code' => $whse_center_code 
            );
	
				$result = $this->Item_model->get_item_by_whse_barcode($data);
	
                echo json_encode($result);
    }

	public function getstock(){
	    //$barcode = $this->input->get('barcode_text');
		$whse_center_code = $this->input->get('center_code');
    
        $data = array(
            //'item_barcode' => $item_barcode,
            'whse_center_code' => $whse_center_code 
            );
				$result = $this->Item_model->get_stock_onhand_by_whse($whse_center_code);		
				$res =  json_encode($result);
                echo $res;
    }

		public function get_transaction(){
	    //$barcode = $this->input->get('barcode_text');
			$username = $this->input->get('username');
			$center_code = $this->input->get('center_code');
		
			$data = array(
			'username' => $username,
            'center_code' => $center_code
            );
				$result = $this->transaction_model->get_transaction_by_user($data);		
				$res =  json_encode($result);
                echo $res;

					
    }


    public function qtyissue(){
		
		// get parameter from mobile
		$item_id = $this->input->get('item_id');
		$qty = $this->input->get('qty');
        $qty_neg = $qty*-1;
		$center_code =  $this->input->get('center_code');
		$username = $this->input->get('username');

		$note = $this->input->get('note');
		$ref_doc = $this->input->get('ref_doc');

        // set default value for field
        $transaction_type = "i";
        $type_reason = '1';
        $unit = '1';
        $qty_convert = '1';

		if ($note==1) {
			$note = "To Customer";
		} elseif ($note==2) {
			$note = "To Spare";
		} elseif (strlen($ref_doc) > 0) {
			$note = "To Customer";
		} else {
            $note = "To Spare";
        }


		//$note = "SCM Mobile App";

        $data = array(
            'item_id' => $item_id,
            'transaction_type' => $transaction_type,
            'type_reason' => $type_reason,
            'qty' => $qty_neg,
            'unit' => $unit,
            'qty_convert' => $qty_convert,
			'note' => $note,
            'center_code' => $center_code,
            'username' => $username ,
			'note' => $note,
			'ref_document' => $ref_doc 
            );

        // insert t_transaction    
        $result = $this->transaction_model->issue($data);
        if($result){
			
			$result = $this->Item_model->get_stock_onhand_by_whse($center_code);		
				// update stock onhand
				$sql = "UPDATE t_item_whse as t1
				JOIN v_item_on_hand as t2 ON t1.item_code = t2.item_id and t1.id_center = t2.id_center
				SET t1.qty_on_hand = t2.qty_balance ;
				";
				$query = $this->db->query($sql);

				$array = array('code'=> 'Success', 'msg'=> 'Issue save Successfully!');
				$result = json_encode($array);

                echo $result;
            //echo "Issue Successfully!";
        }
        else {
				$array = array('code'=> 'Error', 'msg'=> 'Can not save data');
				$result = json_encode($array);

                echo $result;
        }   
    }   
}
