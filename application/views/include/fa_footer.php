 <!-- Jquery Core Js -->
 <script src="<?= base_url('assets/theme_mat')?>/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?= base_url('assets/theme_mat')?>/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src=".<?= base_url('assets/theme_mat')?>/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="<?= base_url('assets/theme_mat')?>/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src=".<?= base_url('assets/theme_mat')?>/js/admin.js"></script>
<script src="<?= base_url('assets/theme_mat')?>/js/pages/examples/sign-in.js"></script>