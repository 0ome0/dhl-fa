﻿<body class="login-page" style="background: #e8e8ea; background-image: url('<?= base_url('assets/images/bg.jpg'); ?>') ; no-repeat; background-background-size:cover;">
    <div class="login-box">
        <div class="imgcontainer" style="text-align:center;">
            <img src="<?php echo base_url('/assets/logo/site_logo.png');?>" class="site_logo" style="width:100%;">
        </div>
        <div class="logo">
        <a href="javascript:void(0);">
            <b><?php echo $this->lang->line('system_name');?></b></a>
            <small><?php echo $this->lang->line('system_name_full');?></small>
        </div>
        <div class="card">
            <div class="body">
            <?php echo form_open(base_url('admin/auth/login'), 'id="sing_in" '); ?>
                    <div class="msg"><?php echo $this->lang->line('login_welcome_msg');?></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        
                    </div>
                    <?php if(isset($msg) || validation_errors() !== ''): ?>
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                        <?= validation_errors();?>
                        <?= isset($msg)? $msg: ''; ?>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <!-- <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label> -->
                            
                        </div>
                        <div class="col-xs-4">
                            <input type="submit" name="submit" id="submit" class="btn btn-block bg-pink waves-effect" value="Submit"> 
                            <!-- <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button> -->
                        </div>
                    </div>
                    <!-- <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">Forgot Password?</a>
                        </div>
                    </div> -->
            <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    
</body>

</html>