<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less --> 
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?= ucwords($this->session->userdata('whse')); ?></a>
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      
     

      <ul class="sidebar-menu">
        
        <li class="header">SERVICE CENTER</li>
        <li><a href="<?= base_url('admin/dashboard/dashboard_user'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li id="transfer" class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Transfer Out</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_user"><a href="<?= base_url('admin/Transfer/transfer_out_pending'); ?>"><i class="fa fa-circle-o"></i>Wait to receive</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/Transfer/transfer_out_completed'); ?>"><i class="fa fa-circle-o"></i>Completed</a></li>
            </ul>
        </li>

        <li id="transfer" class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Transfer Receive</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_user"><a href="<?= base_url('admin/Transfer/transfer_receive_pending'); ?>"><i class="fa fa-circle-o"></i>Wait to receive</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/Transfer/transfer_receive_completed'); ?>"><i class="fa fa-circle-o"></i>Completed</a></li>
            </ul>
        </li>

        <li><a href="<?= base_url('admin/issue/issue_transaction'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Issue Item</span></a></li>
        <li><a href="<?= base_url('admin/receive/receive_transaction'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Receive Item</span></a></li>
        
        
        <li id="transfer" class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Stock Count</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="gen_count_list"><a href="<?= base_url('admin/StockCount/generate_item_list'); ?>"><i class="fa fa-circle-o"></i>Generate Item List</a></li>
              <li id="count_list" class=""><a href="<?= base_url('admin/StockCount/count_list'); ?>"><i class="fa fa-circle-o"></i>Update Count Qty</a></li>
              <li id="counted" class=""><a href="<?= base_url('admin/StockCount/counted_list'); ?>"><i class="fa fa-circle-o"></i>Process Adjust Stock</a></li>
              <!-- <li id="confirm_counted" class=""><a href="<?= base_url('admin/StockCount/confirm_counted'); ?>"><i class="fa fa-circle-o"></i>Counted History</a></li> -->
            </ul>
        </li>
        
        <!-- <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Cancel Transaction</span></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Count Stock</span></a></li> -->

       
        <li class="header">Stock Overview</li>
        <li><a href="<?= base_url('admin/items/item_stock_onhand'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>View Stock Qty Onhand</span></a></li>
        <li><a href="<?= base_url('admin/items/item_transaction'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>View By Transaction</span></a></li>
        <li><a href="<?= base_url('admin/items/item_stock_lower'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Stock lower Alert</span></a></li>

        <li><a href="<?= site_url('admin/auth/logout'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Log out</span></a></li>
      </ul>


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>
