<?php
	class Dashboard_model extends CI_Model{
        
        public function get_item_list_by_whse($whse){
            $sql = "select count(*) as number_item from v_item_whse where whse_center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }
        
        public function get_unassign_owner(){
            $sql = "select count(*) as number_unassign_item from t_item_asset where asset_owner = ''";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_assign_owner(){
            $sql = "select count(*) as number_assign_item from t_item_asset where asset_owner <> ''";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_itasset(){
            $sql = "select count(*) as number_itasset from t_item_asset";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_asset_owner(){
            $sql = "select count(*) as number_asset_owner from t_asset_owner";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_vendor(){
            $sql = "select count(*) as number_vendor from t_vendor";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_asset_expired(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where expire_date < now()";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }
        
        // asset will be expire in 30 days
        public function get_asset_expired_in_1(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 0 and 30 ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        // asset will be expire in 60 days
        public function get_asset_expired_in_2(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 31 and 60 ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        // asset will be expire in 90 days
        public function get_asset_expired_in_3(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 61 and 90 ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

         // get all asset will be expire in query days
        //  public function get_asset_expired_in_3(){
        //     $sql = "select * from  number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 61 and 90 ";
        //     $query = $this->db->query($sql);
		// 	return $result = $query->result_array();
        // }

    }

?>